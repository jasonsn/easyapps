<?php
namespace Rest\Controller;
use Think\Controller;
class DuNiangController extends CommonController {
	
	public function index($id = 0,$page = 1,$limit = 10,$order = 'id desc')
    {
        if (!$id) return;
        $Nav = M('Nav');

        $nav = $Nav->find($id);
        if (!$nav) return;

        $model = D('DuNiang');
        $model->app = $this->appinfo;
        if (!$model) return;

        unset($map);
        $volist  = $model->_list_($map,$page,$limit);
        $volist['nav'] = $nav;
        $this->ajaxReturn($volist,$this->format);
    }

    public function rand($limit = 6){
        $model = D('DuNiang');
        $volist = $model->rand($limit);
        $this->ajaxReturn($volist,$this->format);
    }

    

    
}