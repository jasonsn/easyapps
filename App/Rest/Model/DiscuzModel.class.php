<?php
namespace Rest\Model;
use Think\Model;
class DiscuzModel extends Model
{

    public $app = '';

    protected $connection = 'DB_DISCUZ';

    protected $discuzTableName = 'pre_forum_post';

    public function _list_($nav = '',$map = array() ,$page = 1,$limit = 16, $orderby = ' tid desc'){
        $mmap['fid'] = $nav['pk'];
        $mmap['first'] = 1;
        $list['count'] = $this->where($mmap)->count('id');
        $list['page'] = $page;
        $list['map'] = $mmap;
        $list['limit'] = $limit;
        $pagecount = ceil($list['count'] / $list['limit']);
        if ($pagecount < 1) $pagecount =1;
        $list['pagecount'] = $pagecount;
        $this->trueTableName = $this->discuzTableName;
        $vlist = $this->where($mmap)->page($page,$limit)->order("$orderby ")->select();
        $vvlist = array();
        foreach ($vlist as $k) {
            if (method_exists($this, 'ckvo')) {
                $vvlist[] = $this->ckvo($k);
            }else{
                $vvlist[] = $k;
            }
        }
        $list['volist'] = $vvlist;
        return $list;
    }



    public function getOne($map=array())
    {
        $this->trueTableName = $this->discuzTableName;
        $vo = $this->where($mmap)->find();

        ///查询附件
        // $this->trueTableName = 'pre_forum_attachment';
        // $attr_arr = $this->where($mmap)->select();
        // $attr_ids = '0,';
        // $attrTable = '';
        // foreach ($attr_arr as $k) {
        //     $attr_ids .= $k['aid'].',';
        //     $attrTable = "pre_forum_attachment_". $k['tableid'];
        // }
        // unset($mmap);
        // $mmap['aid'] = array('in',$attr_ids);
        // $mmap['tid'] = $map['id'];
        // $this->trueTableName = $attrTable;
        // $attrs = $this->where($mmap)->select();

        // foreach ($attrs as $k) {
        //     $k['attachment'] = "http://112.124.50.169/data/attachment/forum/".$k['attachment'];
        //     $vo['atts'][] = $k;
        // }
        ///查询附件

        if (method_exists($this, 'ckvo')) {
            $vo = $this->ckvo($vo);
        }
        return $vo;
    }

    public function ckvo($vo = '')
    {
        if (!$vo) return;
        // $this->trueTableName = $this->discuzTableName;
        $vo['message'] = nl2br($vo['message']);
        $vo['message'] = bbcode($vo['message']);
        $vo['picurl'] = $this->app['icon'];

        $mmap['tid'] = $vo['tid'];
        //查询附件
        $this->trueTableName = 'pre_forum_attachment';
        $attr_arr = $this->where($mmap)->select();
        $attr_ids = '0,';
        $attrTable = '';
        foreach ($attr_arr as $k) {
            $attr_ids .= $k['aid'].',';
            $attrTable = "pre_forum_attachment_". $k['tableid'];
        }
        unset($mmap);
        $mmap['aid'] = array('in',$attr_ids);
        $mmap['tid'] = $vo['tid'];
        $this->trueTableName = $attrTable;
        $attrs = $this->where($mmap)->select();

        foreach ($attrs as $k) {
            $k['attachment'] = "http://112.124.50.169/data/attachment/forum/".$k['attachment'];
            $vo['atts'][] = $k;
        }
        //查询附件

        if ($vo['atts']) 
            $vo['picurl'] = $vo['atts'][0]['attachment'];
        return $vo;
    }


}