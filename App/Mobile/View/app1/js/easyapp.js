
Lungo.Service.Settings.async = true;
Lungo.Service.Settings.error = function(type, xhr){
    Lungo.Notification.error('服务器错误', '', 'remove', 3);
};
Lungo.Service.Settings.headers["Content-Type"] = "application/json";
Lungo.Service.Settings.crossDomain = true;
// Lungo.Service.Settings.timeout = 30;

avalon.config({
    debug:false,
})

var duApp = avalon.define("duApp", function(vm){
    vm.currVo = {};
    vm.dataList = [];

    //默认的模板
    // vm.sectionTpl = tplPath+'Home/Index-index.html';
    vm.sectionTpl = '';
    //当前控制器 模块 方法啥的
    vm.Controller = 'Index';
    vm.Module = 'Home';
    vm.Action = 'index';
    //组合得到当前的 section id
    vm.sectionId = '';
    //绑定过的参数 经过处理的
    vm.params = [];

    //是否使用下拉
    vm.pull = "data-pull";


    ///
    vm.indexNavs = [];
    vm.asides = [];
    vm.isasides = false;
    vm.menus = [];
    vm.ismenus = false;
    vm.navs = [];
    vm.isnavs = false;

    vm.startPics = [];
    vm.iStartPics = false;


    //绑定参数
    vm.bindParams = function(params){
        var ps = [];
        for ( var p in params ){
            if (typeof(params[p]) == "undefined") continue;
            ps[p] = params[p];
        }
        ps['toke'] = toke;
        ps['appid'] = appid;
        return ps;
        
    }
    //清空
    vm.clickNav = function(el){
        vm.dataList.removeAll();
    }

    //初始参数和模板
    vm.initTpl = function(params){ 
        
        if (params.widget) {
            vm.sectionTpl = tplPath+'../../Widget/'+params.widget+'/'+params.a+'.html';
        }else{
            vm.sectionTpl = tplPath+params.m+'/'+params.c+'-'+params.a+'.html';
        }
        vm.Module = params.m;
        vm.Controller = params.c;
        vm.Action = params.a;
        vm.sectionId = vm.Module+'-'+vm.Controller+'-'+vm.Action;
    }

    //下拉加载
    vm.pullRefresh = function (){
        var pull = new Lungo.Element.Pull('section>article.active', {
            onPull: "下拉刷新",
            onRelease: "释放获取新数据",
            onRefresh: "获取数据中...",
            callback: function() {
                var hash =  window.location.hash;
                hash = hash.replace('#!/','');
                var hash_arr = hash.split('/');
                var idx = hash_arr.indexOf("page");
                var page = 2;
                if (idx > 0 && hash_arr.length > idx) {
                    page = hash_arr[idx+1];
                    var opage = page;
                    page = parseInt(page) + 1;
                    hash = '/' + hash.replace('page/'+opage,'page/'+page);
                }else{
                    hash = "/"+hash+"/page/" + page;
                }
                avalon.router.navigate(hash);
                location.hash="#!"+hash;
                pull.hide();
            }
        });
    }


})

require(["mmRouter"], function(avalon){

    avalon.router.get("/Widget/:widget/:a", function() {
        this.params.m = 'Widget';
        duApp.initTpl(this.params);
        Lungo.Router.section(duApp.sectionId);
    })

    avalon.router.get("/(:m)(/:c)(/:a)/id(/:id)(/page)(/:page)", function() {
        duApp.initTpl(this.params);
        var ps = duApp.bindParams(this.params);
        Lungo.Notification.show();
        Lungo.Service.json(api, ps, function(resq){
            if (resq['volist']) {
                var vlist = resq['volist'];
                for (var i = 0; i < vlist.length; i++) {
                    duApp.dataList.unshift(vlist[i]) ;
                };
            }
            Lungo.Notification.hide();
            duApp.pullRefresh();
        });
        Lungo.Router.section(duApp.sectionId);
    })

    avalon.router.get("/(:m)(/:c)/view/:id", function() {
        duApp.pull = '';
        this.params['a'] = 'view';
        duApp.initTpl(this.params);
        var ps = duApp.bindParams(this.params);
        Lungo.Notification.show();
        Lungo.Service.json(api, ps, function(resq){
            duApp.currVo = resq;
            Lungo.Notification.hide();
        });
        Lungo.Router.section(duApp.sectionId);
    })

    avalon.router.get("/:m/:c/:a", function() {
        this.params['c'] = 'Nav';
        var ps = duApp.bindParams(this.params);
        Lungo.Service.json(restServer, ps, function(resq){
            duApp.asides = resq['asides'];
            if (resq['asides'].length > 0 ) duApp.isasides = true;
            duApp.navs = resq['navs'];
            if (resq['navs'].length > 0 ) duApp.isnavs = true;
            duApp.menus = resq['menus'];
            if (resq['menus'].length > 0 ) duApp.ismenus = true;

            duApp.startPics = resq['startPics'];
            if (resq['startPics'].length > 0 ) duApp.iStartPics = true;

            duApp.indexNavs = resq['indexNavs'];

        });
    })

    avalon.history.start({
        basepath: basepath,
    })
    avalon.router.navigate("/Home/Index/index")
    avalon.scan();
})


