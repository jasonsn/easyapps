<?php
namespace Mobile\Controller;
use Think\Controller;
class CommonController extends Controller {
    public $appid = 1 ,$app,$uid = 0,$uinfo = '';
    public $tplName = '',$styles;
    public $navs = '';
    public function _initialize()
    {
        $this->appid = 1;
        if (I('appid')) {
            cookie("appid",I('appid'));
        }
        $this->appid = cookie("appid");
        $this->uid = cookie("uid");
        $this->uinfo = cookie("uinfo");
        $AppInfo = D('AppInfo');
        unset($map);
        $map['id'] = $this->appid;
        $this->app = $AppInfo->getOne($map);

        unset($map);
        $Nav = D('Nav');
        $map['appid'] = $this->appid;
        $navs = $Nav->getAll($map);

        if (I('tpl')) {
            $this->app['tpl'] = I('tpl');
        }

        $path = MODULE_PATH.'View/'.$this->app['tpl']."/";
        $this->assign('path',$path);
        $this->assign('app',$this->app);
        $this->assign('appid',$this->appid);

        $this->getStyles();
    }

    //获取样式
    public function getStyles()
    {
        $s_key = 's_styles_'.$this->appid.'_'.$this->app['tpl'];
        // if (S($s_key)) {
        //     $this->assign('styles',S($s_key));
        //     return;
        // }
        $Styles = M('Styles');
        $map['tpl'] = $this->app['tpl'];
        $map['appid'] = $this->appid;
        $volist = $Styles->where($map)->select();
        $styles = array();
        foreach ($volist as $k) {
            if (!$k['content']) continue;

            if (strstr($k['cls'],'background-image')) {
                $k['content'] = "url(".$k['content'].")";
            }



            if (strstr($k['cls'],'index-')) {
                $cls = str_replace("index-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['index'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'menu-')) {
                $cls = str_replace("menu-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['menu'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'aside-')) {
                $cls = str_replace("aside-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['aside'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'nav-')) {
                $cls = str_replace("nav-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['nav'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'title-')) {
                $cls = str_replace("title-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['title'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'body-')) {
                $cls = str_replace("body-", '', $k['cls']);
                $cls = str_replace("body-", '', $cls);
                $styles['body'][$cls] = $k['content'];
            }else{
                $cls = str_replace("title-", '', $k['cls']);
                $styles['global'][$cls] = $k['content'];
            }
        }
        // S($s_key,$styles);

        $this->assign('styles',$styles);
    }


    //检查用户是否已经登录 判断各种移动的  例如 微信 微博 QQ等
    public function checkLogin($value='')
    {
        $isWeixin = false;
        $useragent = addslashes($_SERVER['HTTP_USER_AGENT']);
        //微信里面打开的
        if(strpos($useragent, 'MicroMessenger') )  $isWeixin = true;
    }

    public function getIndexNav()
    {
        $s_key = 's_index_nav_'.$this->appid;
        if (S($s_key)) {
            $this->assign('Indexnavs',S($s_key));
            return;
        }
        $Nav = D('Nav');
        $map['show_index'] = 1;
        $map['appid'] = $this->appid;
        $Indexnavs = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$Indexnavs);
        $this->assign('Indexnavs',$Indexnavs);
    }

    public function getAside()
    {
        $s_key = 's_aside_'.$this->appid;

        if (S($s_key)) {
            $this->assign('asides',S($s_key));
            return;
        }
        $map['pos'] = 'aside';
        $map['appid'] = $this->appid;
        $Nav = D('Nav');
        $asides = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$asides);
        $this->assign('asides',$asides);
    }

    public function getMenu()
    {
        $s_key = 's_menu_'.$this->appid;
        if (S($s_key)) {
            $this->assign('menus',S($s_key));
            return;
        }
        $Nav = D('Nav');
        $map['pos'] = 'menu';
        $map['appid'] = $this->appid;
        $menus = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$menus);
        $this->assign('menus',$menus);
    }

    public function getNav()
    {
        $s_key = 's_nav_'.$this->appid;

        if (S($s_key)) {
            $this->assign('navs',S($s_key));
            return;
        }
        $Nav = D('Nav');
        $map['pos'] = 'bottom';
        $map['appid'] = $this->appid;
        $navs = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$navs);
        $this->assign('navs',$navs);
    }
}