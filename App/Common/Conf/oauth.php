<?php
//定义回调URL通用的URL
define('URL_CALLBACK', 'http://demo.cn/index.php?m=Index&a=callback&type=');
return array(
	//微信登录配置
	'THINK_SDK_WEIXIN' => array(
		'APP_KEY'    => 'wxcd513f6d350f77ec', //应用注册成功后分配的 APP ID
		'APP_SECRET' => 'b17111ed7fd52f239f771f71f6fe83af', //应用注册成功后分配的KEY
		'type'   => 'weixin',
	),
	//腾讯QQ登录配置
	'THINK_SDK_QQ' => array(
		'APP_KEY'    => '', //应用注册成功后分配的 APP ID
		'APP_SECRET' => '', //应用注册成功后分配的KEY
		'CALLBACK'   => URL_CALLBACK . 'qq',
	),
	//新浪微博配置
	'THINK_SDK_SINA' => array(
		'APP_KEY'    => '', //应用注册成功后分配的 APP ID
		'APP_SECRET' => '', //应用注册成功后分配的KEY
		'CALLBACK'   => URL_CALLBACK . 'sina',
	),
);