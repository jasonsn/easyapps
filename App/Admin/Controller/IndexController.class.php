<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends CommonController {
    public function index()
	{
        redirect(U('Admin/Index/chooseApp'));
        // $this->display();
	}
    public function doChangpw()
    {
        $oldpw = I('oldpw');
        $newpw = I('oldpw');
        if (!$oldpw || !$newpw) {
            $this->error('原始密码或新密码必须填写');
            return;
        }
        $Member = M('Member');
        $map['id'] = $this->mid;
        $map['password'] = md5($oldpw);
        $vo = $Member->where($map)->find();
        if (!$vo) {
            $this->error('原始密码不对');
            return;
        }
        $data['password'] = md5($newpw);
        $minfo = $Member->where($map)->save($data);
        if ($minfo !== false) {
            $this->success('密码修改成功',U('Admin/Public/logout'));
            return;
        }else{
            $this->error('未知错误,密码修改不成功');
        }
    }


    public function clearMobileCache()
    {
        $s_key = 's_aside_'.$this->appid;
        S($s_key,null);

        $s_key = 's_index_nav_'.$this->appid;
        S($s_key,null);

        $s_key = 's_nav_'.$this->appid;
        S($s_key,null);

        $s_key = 's_menu_'.$this->appid;
        S($s_key,null);

        $s_key = 's_styles_'.$this->appid.'_'.$this->app['tpl'];
        S($s_key,null);

        $s_key = 's_startpic_'.$this->appid;
        S($s_key,null);

        $data['ret'] = '1';
        $data['msg'] = '成功刷新';
        $this->ajaxReturn($data);
    }

    public function chooseApp()
    {
        $AppInfo = D('AppInfo');
        $map['mid'] = $this->mid;
        $applist = $AppInfo->where($map)->select();
        $this->assign('applist',json_encode($applist));
        // $this->assign('volist',$volist);
        $this->display();
    }

    public function createAppIcon()
    {
        $img = I('img');        
        $appTitle = I('appTitle');
        if (!$img || !$appTitle) return;
        $ext = pathinfo($img);  
        $ext = strtolower($ext["extension"]);
        $icon = "Uploads/Users/icons/app_".$this->appid."_".$this->mid.".".$ext;
        $v = copy($img, $icon);
        // 
        import('@.ORG.WaterMask');
        $waterMask = new WaterMask($icon);
        $waterMask->waterType = 0;
        $waterMask->transparent = 90;
        $waterMask->waterStr = $appTitle;
        $waterMask->fontSize = 38;
        $waterMask->pos = 5;
        $waterMask->output();

        $data['icon'] = $icon;
        $map['id'] = $this->appid;
        $AppInfo = M('AppInfo');
        $AppInfo->where($map)->save($data);

    }

    public function createAppStartpic()
    {
        $img = I('img');        
        $startTitle = I('startTitle');

        $ext = pathinfo($img);  
        $ext = strtolower($ext["extension"]);
        $icon = "Uploads/Users/startpic/app_".$this->appid."_".$this->mid.".".$ext;
        $v = copy($img, $icon);
        import('@.ORG.WaterMask');

        $waterMask = new WaterMask($icon);
        $waterMask->waterType = 1;
        $waterMask->transparent = 90;
        $waterMask->waterImg = $this->appinfo['icon'];
        $waterMask->fontSize = 38;
        $waterMask->pos = 5;
        $waterMask->output();

        if ($startTitle) {
            $waterMask->waterType = 0;
            $waterMask->transparent = 90;
            $waterMask->waterStr = $startTitle;
            $waterMask->fontSize = 38;
            $waterMask->pos = 8;
            $waterMask->output();
        }
        

        $data['start_pic'] = $icon;
        $map['id'] = $this->appid;
        $AppInfo = M('AppInfo');
        $AppInfo->where($map)->save($data);
    }

    public function uploadStartpic()
    {
        import('@.ORG.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize  = 3145728 ;// 设置附件上传大小
        $upload->uploadReplace  = true ;
        $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath =  './Uploads/Users/startpic/';// 设置附件上传目录
        $upload->saveRule  = "app_".$this->appid."_".$this->mid;

        $upload->thumb = true;
        $upload->thumbPrefix = '';
        $upload->thumbMaxWidth = '320';
        $upload->thumbMaxHeight = '480';

        if(!$upload->upload()) {
            $this->error($upload->getErrorMsg());
        }else{
            $info_list =  $upload->getUploadFileInfo();
            $ufile = $info_list[0];
            $dirpath = str_replace("./", "", $ufile['savepath']);
            $url = $dirpath.$ufile['savename'];
            $saveFile = $url;
            $ufile['saveFile'] = $saveFile;
            $ufile['url'] = $url;
            import('@.ORG.Image');
            $data['start_pic'] = $url;
            $map['id'] = $this->appid;
            $AppInfo = M('AppInfo');
            $AppInfo->where($map)->save($data);
        }
        $this->ajaxReturn($ufile);
    }



    public function uploadIcon()
    {
        import('@.ORG.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize  = 3145728 ;// 设置附件上传大小
        $upload->uploadReplace  = true ;
        $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath =  './Uploads/Users/icons/';// 设置附件上传目录
        $upload->saveRule  = "app_".$this->appid."_".$this->mid;

        $upload->thumb = true;
        $upload->thumbPrefix = '';
        $upload->thumbMaxWidth = '128';
        $upload->thumbMaxHeight = '128';

        if(!$upload->upload()) {
            $this->error($upload->getErrorMsg());
        }else{
            $info_list =  $upload->getUploadFileInfo();
            $ufile = $info_list[0];
            $dirpath = str_replace("./", "", $ufile['savepath']);
            $url = $dirpath.$ufile['savename'];
            $saveFile = $url;
            $ufile['saveFile'] = $saveFile;
            $ufile['url'] = $url;
            import('@.ORG.Image');
            $data['icon'] = $url;
            $map['id'] = $this->appid;
            $AppInfo = M('AppInfo');
            $AppInfo->where($map)->save($data);
        }
        $this->ajaxReturn($ufile);
    }
}